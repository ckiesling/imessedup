alias rm='sudo pacman -R --noconfirm'
alias ls='ls --color=auto'
alias ekitty='nvim ~/.config/kitty/kitty.conf'
alias ealacritty='nvim ~/.config/alacritty/alacritty.yml'
alias ehypr='nvim ~/.config/hypr'
alias rmnvim='rm -rf ~/.config/nvim ~/.local/share/nvim'
alias install='yay -S --noconfirm --needed'
alias gcl='git clone --depth1'
alias flip='hyprctl keyword monitor DP-1,preferred,auto,1,transform,1'
alias flipwal='swww img --outputs=DP-1 Pictures/wallpapers/dawngirl.jpg'
alias nv='nvim'
export ZSH="$HOME/.oh-my-zsh"

ZSH_THEME="robbyrussell"

#plugins=(
#  git
#  archlinux
#  zsh-autosuggestions
#  zsh-syntax-highlighting
#)

#source $ZSH/oh-my-zsh.sh

#pokemon-colorscripts --no-title -r
